package eu.software4you.minecraft.publicenderchest;

import eu.software4you.reflect.Parameter;
import eu.software4you.reflect.ReflectUtil;
import eu.software4you.spigot.mappings.Mappings;
import eu.software4you.spigot.multiversion.BukkitReflectionUtils;
import lombok.SneakyThrows;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class Reflect {
    private static Object BLOCK_ENDER_CHEST;

    private static Class<?> CLASS_BLOCK_PACKET;
    private static Class<?> CLASS_BLOCK_POS;
    private static Class<?> CLASS_BLOCK;
    private static Class<?> CLASS_PACKET;

    static {
        init();
    }

    @SneakyThrows
    private static void init() {
        val mapping = Mappings.getMixedMapping();

        CLASS_BLOCK_PACKET = mapping.fromSource("net.minecraft.network.protocol.game.ClientboundBlockEventPacket").find();
        CLASS_BLOCK_POS = mapping.fromSource("net.minecraft.core.BlockPos").find();
        CLASS_BLOCK = mapping.fromSource("net.minecraft.world.level.block.Block").find();
        CLASS_PACKET = mapping.fromSource("net.minecraft.network.protocol.Packet").find();

        BLOCK_ENDER_CHEST = mapping.fromSource("net.minecraft.world.level.block.Blocks")
                .fieldFromSource("ENDER_CHEST").find().get(null);
    }

    @SneakyThrows
    public static void changeChestState(Location loc, boolean open) {
        // construct packet
        Object blockPos = BukkitReflectionUtils.instantiateObject(CLASS_BLOCK_POS,
                loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        Object packet = BukkitReflectionUtils.getConstructor(CLASS_BLOCK_PACKET,
                CLASS_BLOCK_POS, CLASS_BLOCK, int.class, int.class)
                .newInstance(blockPos, BLOCK_ENDER_CHEST, 1, open ? 1 : 0);

        // send packet to players within 64 blocks
        Bukkit.getOnlinePlayers().stream()
                .filter(p -> p.getLocation().distance(loc) <= 64)
                .forEach(all -> {
                    all.playSound(loc, open ? Sound.BLOCK_ENDER_CHEST_OPEN : Sound.BLOCK_ENDER_CHEST_CLOSE, 0.1f, 1);
                    sendPacket(all, packet);
                });
    }

    @SneakyThrows
    private static void sendPacket(Player player, Object packet) {
        Object handle = ReflectUtil.call(player.getClass(), player, "getHandle()");
        Object connection = Mappings.getMixedMapping().fromMapped(handle.getClass().getName())
                .fieldFromSource("connection").find().get(handle);
        ReflectUtil.forceCall(connection.getClass(), connection, "sendPacket()",
                Parameter.single((Class) CLASS_PACKET, packet));
    }
}
