package eu.software4you.minecraft.publicenderchest;

import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class EnderChestHandle implements InventoryHolder, Listener {
    private final Map<Player, Block> openedInventories = new HashMap<>();
    private final Inventory inventory;
    private final List<Block> chests = new ArrayList<>();
    private final PublicEnderChest plugin;
    private final YamlConfiguration yaml;
    private final File inventoryYamlFile;
    private BukkitTask saveTask = null;

    @SneakyThrows
    public EnderChestHandle(PublicEnderChest plugin) {
        this.plugin = plugin;
        this.inventory = Bukkit.createInventory(this, InventoryType.ENDER_CHEST, plugin.getLayout().string("name", "§b<name>"));
        this.inventoryYamlFile = new File(plugin.getDataFolder(), "public_ender_chest.yml");

        yaml = YamlConfiguration.loadConfiguration(inventoryYamlFile);
        ConfigurationSection sec = yaml.getConfigurationSection("contents");
        if (sec != null)
            sec.getKeys(false).forEach(slotStr -> {
                int slot = Integer.parseInt(slotStr);
                inventory.setItem(slot, yaml.getItemStack("contents." + slot));
            });
        sec = yaml.getConfigurationSection("locations");
        if (sec != null)
            sec.getKeys(false).forEach(key -> {
                Block block = yaml.getLocation("locations." + key).getBlock();
                if (block.getType() == Material.ENDER_CHEST)
                    chests.add(block);
            });

    }

    private void saveInventory() {
        plugin.async(() -> {
            for (int slot = 0; slot < inventory.getSize(); slot++) {
                yaml.set("contents." + slot, inventory.getItem(slot));
            }

            saveYaml();
        });
    }

    private void saveYaml() {
        plugin.async(() -> {
            try {
                yaml.save(inventoryYamlFile);
            } catch (IOException e) {
                throw new Error(e);
            }
        });
    }

    public void putChest(Block block) {
        if (isChest(block))
            return;
        chests.add(block);
        yaml.set("locations." + UUID.randomUUID(), block.getLocation());
        saveYaml();
    }

    public void delChest(Block block) {
        chests.remove(block);
        plugin.async(() -> {
            yaml.getConfigurationSection("locations").getKeys(false).forEach(key -> {
                Location loc = yaml.getLocation("locations." + key);
                if (loc.equals(block.getLocation()))
                    yaml.set("locations." + key, null);
            });
            saveYaml();
        });
    }

    public boolean isChest(Block block) {
        return chests.contains(block);
    }

    // handle place / break

    @Override
    public Inventory getInventory() {
        return inventory;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void handlePlace(BlockPlaceEvent e) {
        if (e.isCancelled() || !e.getItemInHand().isSimilar(plugin.getPecItem()))
            return;
        putChest(e.getBlockPlaced());
    }


    // handle inventory opening / closing

    @EventHandler(priority = EventPriority.HIGHEST)
    public void handleBreak(BlockBreakEvent e) {
        if (e.isCancelled())
            return;
        Block block = e.getBlock();
        if (!isChest(block))
            return;
        delChest(block);

        if (!e.isDropItems())
            return;
        e.setDropItems(false);

        var p = e.getPlayer();

        if (p.getGameMode() == GameMode.CREATIVE ||
                p.getInventory().getItemInMainHand().containsEnchantment(Enchantment.SILK_TOUCH)) {
            block.getWorld().dropItemNaturally(block.getLocation(), plugin.getPecItem().clone());
            return;
        }

        block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(Material.SHULKER_SHELL, 2));
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void handleOpen(PlayerInteractEvent e) {
        if (e.useInteractedBlock() == Event.Result.DENY)
            return;
        if (e.getAction() != Action.RIGHT_CLICK_BLOCK)
            return;
        Block clicked = e.getClickedBlock();

        if (clicked == null || !isChest(clicked))
            return;

        Player p = e.getPlayer();
        if (p.isSneaking() && (p.getInventory().getItemInMainHand().getType() != Material.AIR
                || p.getInventory().getItemInOffHand().getType() != Material.AIR)) {
            return;
        }

        e.setCancelled(true);


        if (!openedInventories.containsValue(clicked)) {
            plugin.async(() -> Reflect.changeChestState(clicked.getLocation(), true));
        }

        openedInventories.put(p, clicked);
        p.openInventory(inventory);
    }

    @EventHandler
    public void handleClose(InventoryCloseEvent e) {
        if (e.getInventory() != inventory)
            return;

        Player p = (Player) e.getPlayer();
        if (!openedInventories.containsKey(p))
            return;
        Block block = openedInventories.get(p);
        openedInventories.remove(p);

        if (!openedInventories.containsValue(block)) {
            plugin.async(() -> Reflect.changeChestState(block.getLocation(), false));
        }
    }

    // handle inventory saves

    @EventHandler
    public void handle(InventoryClickEvent e) {
        scheduleInventorySave(e);
    }

    @EventHandler
    public void handle(InventoryDragEvent e) {
        scheduleInventorySave(e);
    }

    @EventHandler
    public void handle(InventoryMoveItemEvent e) {
        scheduleInventorySave(e.getDestination());
    }

    private void scheduleInventorySave(InventoryEvent e) {
        scheduleInventorySave(e.getInventory());
    }

    private void scheduleInventorySave(Inventory inv) {
        if (inv != inventory)
            return;
        if (saveTask != null)
            saveTask.cancel();
        saveTask = plugin.async(() -> {
            saveInventory();
            saveTask = null;
        }, 10);
    }
}
