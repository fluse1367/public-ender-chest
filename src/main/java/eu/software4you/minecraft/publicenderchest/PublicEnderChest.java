package eu.software4you.minecraft.publicenderchest;


import eu.software4you.spigot.item.ItemBuilder;
import eu.software4you.spigot.plugin.ExtendedJavaPlugin;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

public final class PublicEnderChest extends ExtendedJavaPlugin {
    @Getter
    private ItemStack pecItem;
    private ShapedRecipe recipe;
    private EnderChestHandle pec;

    @Override
    public void onEnable() {
        pecItem = new ItemBuilder(Material.ENDER_CHEST)
                .name(getLayout().string("name"))
                .build();

        pec = new EnderChestHandle(this);
        registerEvents(pec);

        recipe = new ShapedRecipe(new NamespacedKey(this, "public_ender_chest"), pecItem);
        recipe.shape(
                "ese",
                "ece",
                "ese"
        );
        recipe.setIngredient('e', Material.EMERALD_BLOCK);
        recipe.setIngredient('s', Material.SHULKER_SHELL);
        recipe.setIngredient('c', Material.ENDER_CHEST);

        Bukkit.getServer().addRecipe(recipe);
    }

    @Override
    public void onDisable() {
        Bukkit.getServer().removeRecipe(recipe.getKey());
    }


}
